<?php
    /*
        Plugin Name: Alphatab
        Plugin URI: http://www.example.com
        Description: Short Code
        Author: FDuarte
        Version: 0.0.9
        Author URI: http://www.example.com
        License: GLP2
        Licence URI: https://www.gnu.org/licenses/gpl-2.0.html
    */

    function create_post_type_alphatab() {
        // Etiquetas para el Post Type
      	$labels = array(
      		'name'                => _x( 'AlphaTabs', 'Post Type General Name', 'guru' ),
      		'singular_name'       => _x( 'AlphaTab', 'Post Type Singular Name', 'guru' ),
      		'menu_name'           => __( 'AlphaTabs', 'guru' ),
      		'parent_item_colon'   => __( 'AlphaTab Parent', 'guru' ),
      		'all_items'           => __( 'All the AlphaTabs', 'guru' ),
      		'view_item'           => __( 'Show Alphatab', 'guru' ),
      		'add_new_item'        => __( 'Add new AlphaTab', 'guru' ),
      		'add_new'             => __( 'Add new AlphaTab', 'guru' ),
      		'edit_item'           => __( 'Edit AlphaTab', 'guru' ),
      		'update_item'         => __( 'Update AlphaTab', 'guru' ),
      		'search_items'        => __( 'Search AlphaTab', 'guru' ),
      		'not_found'           => __( 'Not found', 'guru' ),
      		'not_found_in_trash'  => __( 'Not found in the trash', 'guru' ),
      	);

        //Otras opciones para el post type

        $args = array(
          'label'       => __('alphatabs', 'gourmet-artist'),
          'description' => __('Alphatabs exercises', 'gourmet-artist'),
          'labels'      => $labels,
          'supports'    => array('title', 'editor',  'thumbnail',  'revisions', ),
          'hierarchical' => false,
          'public'      => true,
          'show_ui'     => true,
          'show_in_menu' => true,
          'show_in_admin_bar' => true,
          'menu_position' => 6,
          'menu_icon' => 'dashicons-book-alt',
          'can_export' => true,
          'has_archive' => true,
          'exclude_from_search' => false,
          'capibility_type' => 'page',
        );
        //registrar post type
        register_post_type( 'alphatabs', $args );
    }
    add_action('init', 'create_post_type_alphatab', 0);

    function alphatab_scripts_with_jquery(){
        // Register the scripts for the plugin:
        wp_register_script( 'AlphaTab-min-script', plugins_url( '/js/AlphaTab.js', __FILE__ ), array( 'jquery' ) );
        wp_enqueue_script( 'AlphaTab-min-script' );
        wp_register_script( 'jquery.alphaTab-script', plugins_url( '/js/jquery.alphaTab.js', __FILE__ ), array( 'jquery' ) );
        wp_enqueue_script( 'jquery.alphaTab-script' );
        wp_register_script( 'swfobject-script', plugins_url( '/js/swfobject.js', __FILE__ ), array( 'jquery' ) );
        wp_enqueue_script( 'swfobject-script' );
        wp_register_script( 'AlphaSynth-script', plugins_url( '/js/AlphaSynth.js', __FILE__ ), array( 'jquery' ) );
        wp_enqueue_script( 'AlphaSynth-script' );
        wp_register_script( 'jquery.alphaTab.alphaSynth-script', plugins_url( '/js/jquery.alphaTab.alphaSynth.js', __FILE__ ), array( 'jquery' ) );
        wp_enqueue_script( 'jquery.alphaTab.alphaSynth-script' );
    }
    add_action( 'wp_enqueue_scripts', 'alphatab_scripts_with_jquery' );

    function alphatab_styles(){
        // Register the styles for the plugin:
        wp_register_style( 'alphatab-style', plugins_url( '/css/styles.css', __FILE__ ), array(), '1.0','all' );
        wp_enqueue_style('alphatab-style' );
    }
    add_action( 'wp_enqueue_scripts', 'alphatab_styles' );

    /* ========== START REGISTER CUSTOM POST TYPE TEMPLATE ========== */
    if( !function_exists('get_alphatab_template') ):
       function get_alphatab_template($single_template) {
            global $wp_query, $post;
            if ($post->post_type == 'alphatabs'){
                $single_template = plugin_dir_path(__FILE__) . 'alphatab-template.php';
            }//end if alphatab-template
            return $single_template;
        }//end get_alphatab_template function
    endif;

    add_filter( 'single_template', 'get_alphatab_template' ) ;
    /* ========== END REGISTER CUSTOM POST TYPE TEMPLATE ========== */

    /* ========== START REGISTER CUSTOM METABOX ========== */
    function add_custom_meta_boxes() {
        // Define the custom attachment for alphatabs
        add_meta_box('gpx_attachment','GPX Attachment','gpx_attachment','alphatabs','side');

    } // end add_custom_meta_boxes
    add_action('add_meta_boxes', 'add_custom_meta_boxes');
    /* ========== END REGISTER CUSTOM METABOX ========== */

    //CUSTOM METABOX REGISTER FUNCTION
    function gpx_attachment($post) {
      $values = get_post_custom( $post->ID );
	    //$text = isset( $values['my_meta_box_text'] ) ? esc_attr( $values['my_meta_box_text'][0] ) : '';
      wp_nonce_field(plugin_basename(__FILE__), 'gpx_attachment_nonce');
      $html = '<p class="description">';
          $html .= 'Upload your GPX here.';
      $html .= '</p>';
      $html .= '<input type="file" id="gpx_attachment" name="gpx_attachment" value="" size="25" />';
      $html .= '<p>Current File: '. $values['gpx_attachment-name'][0] .'<p/>';
      echo $html;
    } // end gpx_attachment

    function save_custom_meta_data($id){

        /* --- security verification --- */
        if(!wp_verify_nonce($_POST['gpx_attachment_nonce'], plugin_basename(__FILE__))) {
          return $id;
        } // end if

        if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
          return $id;
        } // end if

        if('page' == $_POST['post_type']) {
          if(!current_user_can('edit_page', $id)) {
            return $id;
          } // end if
        } else {
            if(!current_user_can('edit_page', $id)) {
                return $id;
            } // end if
        } // end if
        /* - end security verification - */

        // Make sure the file array isn't empty
        if(!empty($_FILES['gpx_attachment']['name'])) {

            // Setup the array of supported file types. In this case, it's just PDF.
            $supported_types = array('application/gpx+xml');

            // Get the file type of the upload
            $arr_file_type = wp_check_filetype(basename($_FILES['gpx_attachment']['name']));
            $uploaded_type = $arr_file_type['type'];

            // Check if the type is supported. If not, throw an error.
            if(in_array($uploaded_type, $supported_types)) {

                // Use the WordPress API to upload the file
                $upload = wp_upload_bits($_FILES['gpx_attachment']['name'], null, file_get_contents($_FILES['gpx_attachment']['tmp_name']));

                if(isset($upload['error']) && $upload['error'] != 0) {
                    wp_die('There was an error uploading your file. The error is: ' . $upload['error']);
                } else {
                    add_post_meta($id, 'gpx_attachment', $upload);
                    add_post_meta($id, 'gpx_attachment-name', $_FILES['gpx_attachment']['name']);
                    update_post_meta($id, 'gpx_attachment', $upload);
                    update_post_meta($id, 'gpx_attachment-name', $_FILES['gpx_attachment']['name']);
                } // end if/else

            } else {
              $f = fopen($_FILES['gpx_attachment']['tmp_name'], "rb");
              fseek($f, 1);
              $in = fgets($f, 19);
              fclose($f);

              if ($in == 'FICHIER GUITAR PRO') {
                // Use the WordPress API to upload the file
                $upload = wp_upload_bits($_FILES['gpx_attachment']['name'], null, file_get_contents($_FILES['gpx_attachment']['tmp_name']));

                if(isset($upload['error']) && $upload['error'] != 0) {
                    wp_die('There was an error uploading your file. The error is: ' . $upload['error']);
                } else {
                    add_post_meta($id, 'gpx_attachment', $upload);
                    add_post_meta($id, 'gpx_attachment-name', $_FILES['gpx_attachment']['name']);
                    update_post_meta($id, 'gpx_attachment', $upload);
                    update_post_meta($id, 'gpx_attachment-name', $_FILES['gpx_attachment']['name']);
                } // end if/else
              }else{
                  wp_die("The file type that you've uploaded is not a GPX.");
              }
            } // end if/else

        } // end if

    } // end save_custom_meta_data
    add_action('save_post', 'save_custom_meta_data');

    function update_edit_form() {
        echo ' enctype="multipart/form-data"';
    } // end update_edit_form
    add_action('post_edit_form_tag', 'update_edit_form');

?>
