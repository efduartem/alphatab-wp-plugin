<?php
get_header(); ?>

    <div class="alphatab-container">
      <!-- Player controls -->
      <ul class="alphatab-player-ul">
          <li class="alphatab-player-li"><span>Speed: </span></li>
          <li class="alphatab-player-li-select">
              <select id="alphatab-speed-select">
                <option value="0.25">25%</option>
                <option value="0.5">50%</option>
                <option value="0.6">60%</option>
                <option value="0.7">70%</option>
                <option value="0.8">80%</option>
                <option value="0.9">90%</option>
                <option value="1" selected>100%</option>
                <option value="1.1">110%</option>
                <option value="1.25">125%</option>
                <option value="1.25">125%</option>
                <option value="1.5">150%</option>
                <option value="2">200%</option>
              </select>
          </li>
          <li class="alphatab-player-li">
              <input type="checkbox" name="scroolling" value="scroolling" id="scroolling" checked>
              <label for="scroolling">Scrool</label>
          </li>
          <li class="alphatab-player-li">
              <input type="checkbox" name="looping" value="looping" id="looping">
              <label for="looping">Loop</label>
          </li>
          <li id="soundFontProgressMenuItem" class="alphatab-player-li">
              <span>SoundFont</span>
              <div id="soundFontProgress">
                  0%
              </div>
          </li>
      </ul>
      <div id="controls">
        <button id="playPause" disabled="disabled" class="alphatab-btn">Play</button>
        <button id="stop" disabled="disabled" class="alphatab-btn">Stop</button>
      </div>
      <?php
          $excersiceFile = get_post_meta(get_the_ID(), 'wp_custom_attachment', true);
          $sondFontUrl = plugins_url( 'files/default.sf2', __FILE__ );
      ?>
      <div id="alphaTab"
          data-file="<?php echo $excersiceFile['url'];?>"
          data-player="<?php echo $sondFontUrl;?>"
          data-player-offset="[-10,-70]"></div>
    </div>

        <script type="text/javascript">
            (function ($) {
                var at = $('#alphaTab');
                //
                // 1. setup events
                at.on('alphaTab.soundFontLoad', function(e, progress) {
                    var percentage = ((progress.loaded / progress.total) * 100) | 0;
                    $('#soundFontProgress').css('width', percentage + '%').text(percentage + '%');
                });
                at.on('alphaTab.soundFontLoaded', function(){
                    $('#soundFontProgressMenuItem').hide();
                });
                at.on('alphaTab.playerReady', function(){
                    $('#loadingInfo').hide();
                    $('#playPause').prop('disabled', false).removeAttr('disabled');
                    $('#stop').prop('disabled', false).removeAttr('disabled');
                    $('#looping').prop('disabled', false).removeAttr('disabled');
                    updateControls();
                });
                at.on('alphaTab.playerStateChanged', function () {
                    updateControls();
                });

                //
                // 2. Load alphaTab
                at.alphaTab();
                at.alphaTab('playerOptions',{scrollOffset:-200})

                //
                // 3. Setup UI controls and use API to control the playback
                $('#playPause').click(function () {
                    at.alphaTab('playPause');
                });

                $('#stop').click(function () {
                    at.alphaTab('stop');
                });

                $("#scroolling").on('click', function(){
                    if($(this).is(":checked")){
                        at.alphaTab('playerOptions',{autoScroll:'vertical', scrollOffset:-200})
                    }else{
                        at.alphaTab('playerOptions',{autoScroll:''})
                    }
                })

                $('#looping').click(function(e){
                //     e.preventDefault();
                    // var looping = !at.alphaTab('loop');
                    // console.log(looping);
                    // at.alphaTab('loop', looping);
                    if($(this).is(":checked")){
                        at.alphaTab('loop', true);
                    }else{
                        at.alphaTab('loop', false);
                    }
                    // if ($(this).is(":checked")){
                    //     $('#looping').closest('li').addClass('active');
                    // }else{
                    //     $('#looping').closest('li').removeClass('active');
                    // }
                });

                $("#alphatab-speed-select").on("change", function(){
                    var playbackSpeed = $(this).val();
                    at.alphaTab('playbackSpeed', playbackSpeed);

                    // $("#alphatab-speed-select option:selected").each(function(){
                    //   $('#playbackSpeed').text($(this).text());
                    // });
                })

                function updateControls() {
                    var playerState = at.alphaTab('playerState');
                    switch (playerState) {
                        case 0: // stopped/paused
                            $("#playPause").text("Play");
                            $('#playPause').removeClass('glyphicon-pause').addClass('glyphicon-play');
                            break;
                        case 1: // playing
                            $("#playPause").text("Pause");
                            $('#playPause').removeClass('glyphicon-play').addClass('glyphicon-pause');
                            break;
                    }
                }

                //$('a[data-layout]').click(function (e) {
                    //alert("Test Click")
                    // $('a[data-layout]').closest('li').removeClass('active');
                    // $(this).closest('li').addClass('active');
                    //
                    // e.preventDefault();
                    //
                    // var layout = $(this).data('layout');
                    // var scrollmode = $(this).data('scrollmode');
                    //
                    // at.removeClass('horizontal page');
                    // at.addClass(layout);
                    //
                    // // update renderer
                    // at.alphaTab('layout', layout);
                    //
                    // // update player
                    // at.alphaTab('autoScroll', scrollmode);
                    // $('body,html').animate({
                    //     scrollTop: 0
                    // }, 300);
                //});
            })(jQuery);
        </script>

<?php
get_footer();
